package com.example.sectors.controllers;

import com.example.sectors.domain.Sector;
import com.example.sectors.dto.SectorDto;
import com.example.sectors.repository.SectorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@CrossOrigin
@RequiredArgsConstructor
public class SectorsController {

    private final SectorRepository repository;

    @GetMapping("/sectors")
    public List<SectorDto> getSectors() {
        Iterable<Sector> sectorsIterable = repository.findAll();
        return StreamSupport.stream(sectorsIterable.spliterator(), false)
                .map(this::toSectorDto)
                .collect(Collectors.toList());
    }

    private SectorDto toSectorDto(Sector sector) {
        return SectorDto.builder()
                .name(sector.getName())
                .level(sector.getLevel())
                .subSectors(sector.getChildSectors().stream().map(this::toSectorDto).toList())
                .build();
    }
}
