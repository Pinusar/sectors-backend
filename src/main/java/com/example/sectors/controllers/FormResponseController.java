package com.example.sectors.controllers;

import com.example.sectors.domain.FormResponse;
import com.example.sectors.domain.Sector;
import com.example.sectors.domain.User;
import com.example.sectors.dto.FormResponseDto;
import com.example.sectors.repository.FormResponseRepository;
import com.example.sectors.repository.SectorRepository;
import com.example.sectors.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.text.MessageFormat;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@CrossOrigin
@RequiredArgsConstructor
public class FormResponseController {

    private final FormResponseRepository formResponseRepository;
    private final UserRepository userRepository;
    private final SectorRepository sectorRepository;

    @GetMapping("/responses/{user}")
    public ResponseEntity<?> getLatest(@PathVariable String user) {
        return getLatestForUser(user)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping("/responses")
    public ResponseEntity<?> insertFormResponse(@Validated @RequestBody FormResponseDto formResponse, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.badRequest().body(
                    ErrorResponse.builder()
                            .errors(getErrors(bindingResult))
                            .build()
            );
        }
        formResponseRepository.save(mapToEntity(formResponse));
        return ResponseEntity.ok(
                mapToDto(getLatestForUser(formResponse.getUser())
                        .orElseThrow(RuntimeException::new)));
    }

    private static List<String> getErrors(BindingResult bindingResult) {
        return bindingResult.getAllErrors().stream()
                .map(e -> {
                    if (e instanceof FieldError fieldError) {
                        return MessageFormat.format("{0} {1}", fieldError.getField(), e.getDefaultMessage());
                    }
                return e.getDefaultMessage();
                })
                .collect(Collectors.toList());
    }

    private FormResponseDto mapToDto(FormResponse formResponse) {
        return FormResponseDto.builder()
                .user(formResponse.getRespondent().getName())
                .name(formResponse.getName())
                .agreeToTerms(formResponse.isAgreeToTerms())
                .sectors(formResponse.getSectors().stream().map(Sector::getName).collect(Collectors.toList()))
                .build();
    }

    public Optional<FormResponse> getLatestForUser(String userName) {
        Iterable<FormResponse> all = formResponseRepository.findAll();
        return StreamSupport.stream(all.spliterator(), false)
                .filter(r -> r.getRespondent().getName().equals(userName))
                .max(Comparator.comparing(FormResponse::getCreatedAt));
    }

    private FormResponse mapToEntity(FormResponseDto dto) {
        Iterable<User> userIterable = userRepository.findAll();
        User user = StreamSupport.stream(userIterable.spliterator(), false)
                .filter(u -> u.getName().equals(dto.getUser()))
                .findFirst()
                .orElseThrow(RuntimeException::new);

        Iterable<Sector> sectorIterable = sectorRepository.findAll();
        List<Sector> sectors = StreamSupport.stream(sectorIterable.spliterator(), false)
                .filter(s -> dto.getSectors().contains(s.getName()))
                .collect(Collectors.toList());

        return FormResponse.builder()
                .id(UUID.randomUUID().toString())
                .respondent(user)
                .name(dto.getName())
                .sectors(sectors)
                .agreeToTerms(dto.isAgreeToTerms())
                .build();
    }

    @AllArgsConstructor
    @Builder
    @Data
    private static class ErrorResponse {
        List<String> errors;
    }
}
