package com.example.sectors.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import java.time.Instant;
import java.util.Collection;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FormResponse {
    @Id
    public String id;
    @ManyToOne
    public User respondent;
    @ManyToMany
    public Collection<Sector> sectors;
    public String name;
    public boolean agreeToTerms;
    @CreationTimestamp
    private Instant createdAt;
}
