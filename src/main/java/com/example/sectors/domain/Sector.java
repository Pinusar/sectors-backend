package com.example.sectors.domain;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Sector {
    public String name;
    @Id
    public String id;

    @ManyToOne
    @JoinColumn(name = "parent_id")
    private Sector parent;

    @OneToMany
    private List<Sector> childSectors;

    @Transient
    private int level;

    public int getLevel() {
        if (this.parent == null) {
            return 0;
        }
        return this.parent.getLevel() + 1;
    }
}
