package com.example.sectors.repository;

import com.example.sectors.domain.FormResponse;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FormResponseRepository extends CrudRepository<FormResponse, Long> {
}