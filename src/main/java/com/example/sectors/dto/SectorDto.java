package com.example.sectors.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class SectorDto {
    public String name;
    public int level;
    public List<SectorDto> subSectors;
}
