package com.example.sectors.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class FormResponseDto {
    @NotBlank
    public String user;
    @NotBlank
    public String name;
    @NotEmpty
    public List<String> sectors;
    public boolean agreeToTerms;
}
